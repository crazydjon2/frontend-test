## Refactoring

Task to review and improve existing code.

Please review the code below:

```javascript

function func(s, a, b) {

	if (s.match(/^$/)) { // Unnesassary regex for checling empty string replace by !s.length
		return -1;
	}
	
    // Old var replace to let
	var i = s.length -1;
	var aIndex =     -1; // replace let aIndex, bIndex = -1
	var bIndex =     -1;
	
	while ((aIndex == -1) && (bIndex == -1) && (i > 0)) { // replace == to ===
	    if (s.substring(i, i +1) == a) {
	    	aIndex = i;
    	}
	    if (s.substring(i, i +1) == b) {
	    	bIndex = i;
    	}
	    i = i - 1;
	}
	
    // unnessasry if replace by return Math.max(aIndex, bIndex)
	if (aIndex != -1) {
	    if (bIndex == -1) {
	        return aIndex;
	    }
	    else {
	        return Math.max(aIndex, bIndex);
	    }
	}

	if (bIndex != -1) {
	    return bIndex;
	}
	else {
	    return -1;
	}
}
```
## Refactoring done

```javascript
// All of this can replaced by lastIndexOf
function func(s, a, b) {
	if (!s.length) {
		return -1;
	}
    return Math.max(s.lastIndexOf(a), s.lastIndexOf(b));
}
```


# frontEndTest

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

For this task i pick Nuxt framework, wich was utility tools for Vue, easy ssr, easy routing. All request was mocked. 
