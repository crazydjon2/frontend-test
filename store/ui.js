export const state = () => ({
    showPreload: false
})

export const mutations = {
    SET_SHOW_PRELOAD(state, bool) {
        state.showPreload = bool;
    }
}

export const getters = {
    showPreload: state => state.showPreload
}
