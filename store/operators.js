export const state = () => ({
    operators: [],
    operator: {},
    status: null
})

export const mutations = {
    SET_OPERATORS(state, operators) {
        state.operators = operators;
    },
    SET_OPERATOR(state, operator) {
        state.operator = operator;
    },
    SET_STATUS(state, status) {
        state.status = status;
    }
}

export const getters = {
    operators: state => state.operators,
    operator: state => state.operator,
    status: state => state.status
}

export const actions = {
    // Request for getting all operators
    GET_OPERATORS({ commit }) {
        // Put axios/ajax requets for getting operators and set like like here
        commit('SET_OPERATORS', [
            {
                id: 1,
                slug: 'mts',
                name: 'MTS'
            },
            {
                id: 2,
                slug: 'beeline',
                name: 'Beeline'
            },
            {
                id: 3,
                slug: 'megafon',
                name: 'Megafon'
            }
        ]);
    },
    // Request for getting operator by slug
    GET_OPERATOR_BY_SLUG({ commit, state }, slug) {
         // Put axios/ajax  requets for getting operator by slug and set like like here
         const currentOperator = state.operators.find(o => o.slug === slug)
         commit('SET_OPERATOR', {
             id: currentOperator.id,
             slug: slug,
             name: currentOperator.name,
             description: `Description about ${currentOperator.name}`
         });
    },
    // Request for fill number
    FILL_PHONE({ commit }, { slug, phone, amount }) {
        const status = Math.random() < 0.5;
        setTimeout(() => {
            if (status) {
                commit('SET_STATUS', 'done');
            } else {
                commit('SET_STATUS', 'error');
            }
            commit('ui/SET_SHOW_PRELOAD', false, { root: true })
        }, 1500);
    }
}
