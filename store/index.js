export const state = () => ({
    operators: []
})

export const mutations = {
    SET_POSTS (state, posts) {
        state.posts = posts
    }
}

export const getters = {
    posts: state => state.posts
}

// SSR requests, start with app launch. 
export const actions = {
    async nuxtServerInit ({ commit, dispatch }, { $axios, error, req, app }) {
        await dispatch('operators/GET_OPERATORS')
    }
}